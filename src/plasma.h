#ifndef PLASMA_H
#define PLASMA_H

#include <cstdlib>
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

class plasma
{
    public:

        plasma();

        enum Granularity{
            Fine,
            Medium,
            Big,
        };

        void update();
        uint32_t getPixel(int x, int y, Granularity gran);

    private:
        float shift;
        
        static const int BMP_SIZE = 16;
        static constexpr float offset = UINT16_MAX / 2;

};

#endif
