#ifndef FONT_H
#define FONT_H

#include <ctype.h>
#include <Arduino.h>

class font{
    public:

        static const int letterWidth = 3;
        static const int thinLetterWidth = 3;
        static const int letterHeight = 5;

        typedef bool (letter)[letterWidth][letterHeight];
        typedef bool (thinLetter)[thinLetterWidth][letterHeight];

        static const thinLetter Space;
        static const thinLetter Colon;
     
        static const letter Space_Monospaced;
        static const letter Degree;
        static const letter Percent;
        static const letter Dash;
        static const letter Colon_Monospaced;

        static const letter Zero;
        static const letter One;
        static const letter Two;
        static const letter Three;
        static const letter Four;
        static const letter Five;
        static const letter Six;
        static const letter Seven;
        static const letter Eight;
        static const letter Nine;

        static const letter A;
        static const letter B;
        static const letter C;
        static const letter D;
        static const letter E;
        static const letter F;
        static const letter G;
        static const letter H;
        static const letter I;
        static const letter J;
        static const letter K;
        static const letter L;
        static const letter M;
        static const letter N;
        static const letter O;
        static const letter P;
        static const letter Q;
        static const letter R;
        static const letter S;
        static const letter T;
        static const letter U;
        static const letter V;
        static const letter W;
        static const letter X;
        static const letter Y;
        static const letter Z;

        static const letter* getNumber(int number);

        static const letter* getLetter(char letter);
        
};

#endif
