#include "font.h"

const font::letter* font::getNumber(int number){

    switch(number){
        case 0:
            return &Zero;
        case 1:
            return &One;
        case 2:
            return &Two;
        case 3:
            return &Three;
        case 4:
            return &Four;
        case 5:
            return &Five;
        case 6:
            return &Six;
        case 7:
            return &Seven;
        case 8:
            return &Eight;
        case 9:
            return &Nine;
        default:
            return &E;
    }
}

const font::letter* font::getLetter(char letter){
    switch(toupper(letter)){
        case ' ':
            return &Space_Monospaced;
        case '%':
            return &Percent;
        case '-':
            return &Dash;
        case ':':
            return &Colon_Monospaced;
        case 'A':
            return &A;
        case 'B':
            return &B;
        case 'C':
            return &C;
        case 'D':
            return &D;
        case 'E':
            return &E;
        case 'F':
            return &F;
        case 'G':
            return &G;
        case 'H':
            return &H;
        case 'I':
            return &I;
        case 'J':
            return &J;
        case 'K':
            return &K;
        case 'L':
            return &L;
        case 'M':
            return &M;
        case 'N':
            return &N;
        case 'O':
            return &O;
        case 'P':
            return &P;
        case 'Q':
            return &Q;
        case 'R':
            return &R;
        case 'S':
            return &S;
        case 'T':
            return &T;
        case 'U':
            return &U;
        case 'V':
            return &V;
        case 'W':
            return &W;
        case 'X':
            return &X;
        case 'Y':
            return &Y;
        case 'Z':
            return &Z;
        case '0':
            return &Zero;
        case '1':
            return &One;
        case '2':
            return &Two;
        case '3':
            return &Three;
        case '4':
            return &Four;
        case '5':
            return &Five;
        case '6':
            return &Six;
        case '7':
            return &Seven;
        case '8':
            return &Eight;
        case '9':
            return &Nine;
        default:
            return &Space_Monospaced;
    }
}

const font::thinLetter font::Space = {
    {false, false, false, false, false},
};

const font::thinLetter font::Colon = {
    {false, true, false, true, false},
};

const font::letter font::Space_Monospaced = {
    {false, false, false, false, false},
    {false, false, false, false, false},
    {false, false, false, false, false},
};

const font::letter font::Degree = {
    {false, true, false, false, false},
    {true, false, true, false, false},
    {false, true, false, false, false},
};

const font::letter font::Percent = {
    {true, false, false, true, false},
    {false, false, true, false, false},
    {false, true, false, false, true},
};

const font::letter font::Dash = {
    {false, false, true, false, false},
    {false, false, true, false, false},
    {false, false, true, false, false},
};

const font::letter font::Colon_Monospaced = {
    {false, false, false, false, false},
    {false, true, false, true, false},
    {false, false, false, false, false},
};

const font::letter font::Zero = {
    {true, true, true, true, true},
    {true, false, false, false, true},
    {true, true, true, true, true},
};

const font::letter font::One = {
    {true, false, false, false, true},
    {true, true, true, true, true},
    {false, false, false, false, true},
};

const font::letter font::Two = {
    {true, false, true, true, true},
    {true, false, true, false, true},
    {true, true, true, false, true},
};

const font::letter font::Three = {
    {true, false, false, false, true},
    {true, false, true, false, true},
    {true, true, true, true, true},
};

const font::letter font::Four = {
    {true, true, true, false, false},
    {false, false, true, false, false},
    {true, true, true, true, true},
};

const font::letter font::Five = {
    {true, true, true, false, true},
    {true, false, true, false, true},
    {true, false, true, true, true},
};

const font::letter font::Six = {
    {true, false, true, true, true},
    {true, false, true, false, true},
    {true, true, true, true, true},
};

const font::letter font::Seven = {
    {true, false, false, false, false},
    {true, false, false, false, false},
    {true, true, true, true, true},
};

const font::letter font::Eight = {
    {true, true, true, true, true},
    {true, false, true, false, true},
    {true, true, true, true, true},
};

const font::letter font::Nine = {
    {true, true, true, false, false},
    {true, false, true, false, false},
    {true, true, true, true, true},
};

const font::letter font::A = {
    {true, true, true, true, true},
    {true, false, true, false, false},
    {true, true, true, true, true},
};

const font::letter font::B = {
    {true, true, true, true, true},
    {true, false, true, false, true},
    {false, true, true, true, false},
};

const font::letter font::C = {
    {false, true, true, true, false},
    {true, false, false, false, true},
    {true, false, false, false, true},
};

const font::letter font::D = {
    {true, true, true, true, true},
    {true, false, false, false, true},
    {false, true, true, true, false},
};

const font::letter font::E = {
    {true, true, true, true, true},
    {true, false, true, false, true},
    {true, false, false, false, true},
};

const font::letter font::F = {
    {true, true, true, true, true},
    {true, false, true, false, false},
    {true, false, false, false, false},
};

const font::letter font::G = {
    {true, true, true, true, true},
    {true, false, false, false, true},
    {true, false, false, true, true},
};

const font::letter font::H = {
    {true, true, true, true, true},
    {false, false, true, false, false},
    {true, true, true, true, true},
};

const font::letter font::I = {
    {true, false, false, false, true},
    {true, true, true, true, true},
    {true, false, false, false, true},
};

const font::letter font::J = {
    {true, false, false, false, true},
    {true, true, true, true, true},
    {true, false, false, false, false},
};

const font::letter font::K = {
    {true, true, true, true, true},
    {false, false, true, false, false},
    {true, true, false, true, true},
};

const font::letter font::L = {
    {true, true, true, true, true},
    {false, false, false, false, true},
    {false, false, false, false, true},
};

const font::letter font::M = {
    {true, true, true, true, true},
    {false, true, false, false, false},
    {true, true, true, true, true},
};

const font::letter font::N = {
    {false, true, true, true, true},
    {false, true, false, false, false},
    {true, true, true, true, true},
};

const font::letter font::O = {
    {false, true, true, true, false},
    {true, false, false, false, true},
    {false, true, true, true, false},
};

const font::letter font::P = {
    {true, true, true, true, true},
    {true, false, true, false, false},
    {false, true, false, false, false},
};

const font::letter font::Q = {
    {false, true, true, true, false},
    {true, false, false, false, true},
    {false, true, true, true, true},
};

const font::letter font::R = {
    {true, true, true, true, true},
    {true, false, true, false, false},
    {false, true, false, true, true},
};

const font::letter font::S = {
    {true, true, true, false, true},
    {true, false, true, false, true},
    {true, false, true, true, true},
};

const font::letter font::T = {
    {true, false, false, false, false},
    {true, true, true, true, true},
    {true, false, false, false, false},
};

const font::letter font::U = {
    {true, true, true, true, false},
    {false, false, false, false, true},
    {true, true, true, true, true},
};

const font::letter font::V = {
    {true, true, true, true, false},
    {false, false, false, false, true},
    {true, true, true, true, false},
};

const font::letter font::W = {
    {true, true, true, true, true},
    {false, false, false, true, false},
    {true, true, true, true, true},
};

const font::letter font::X = {
    {true, true, false, true, true},
    {false, false, true, false, false},
    {true, true, false, true, true},
};

const font::letter font::Y = {
    {true, true, false, false, false},
    {false, false, true, true, true},
    {true, true, false, false, false},
};

const font::letter font::Z = {
    {true, false, true, true, true},
    {true, false, true, false, true},
    {true, true, true, false, true},
};
