#include <Adafruit_NeoPixel.h>
#include <string>
#include <cstdlib>
#include <sys/time.h>
#include <math.h>
#include <ezTime.h>
#include "font.h"
#include "config.h"
#include "plasma.h"

#include <WiFi.h>
extern "C" {
  #include "freertos/FreeRTOS.h"
  #include "freertos/timers.h"
}
#include <AsyncMqttClient.h>

#define PIN 27

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(DIMENSION_X * DIMENSION_Y, PIN, NEO_GRB + NEO_KHZ800);

int currentMins = 0;
int currentHs = 0;
int currentTemp = 21;
int lastDisplayedTemp = 0;
int currentHum = 42;
int lastDisplayedHum = 0;

plasma* myPlasma;

enum DisplayMode{
  SolidColor,
  Rainbow,
  Plasma,
  InvertedSolidColor,
  InvertedRainbow,
  InvertedPlasma,
  PurePlasma,
};

#define SOLID_COLOR "SOLID_COLOR"
#define RAINBOW "RAINBOW"
#define PLASMA "PLASMA"
#define INVERTED_SOLID_COLOR "INVERTED_SOLID_COLOR"
#define INVERTED_RAINBOW "INVERTED_RAINBOW"
#define INVERTED_PLASMA "INVERTED_PLASMA"
#define PURE_PLASMA "PURE_PLASMA"

#define GRAN_FINE "GRAN_FINE"
#define GRAN_MEDIUM "GRAN_MEDIUM"
#define GRAN_BIG "GRAN_BIG"

typedef uint32_t (*colorFunc)(int, int);

DisplayMode currentDisplayMode = DisplayMode::InvertedPlasma;
uint32_t currentColor = strip.Color(0, 255, 0);
plasma::Granularity currentGranularity = plasma::Granularity::Medium;
uint32_t currentDelay = 0;

AsyncMqttClient mqttClient;
TimerHandle_t mqttReconnectTimer;
TimerHandle_t wifiReconnectTimer;

Timezone timezone;

void connectToWifi() {
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void WiFiEvent(WiFiEvent_t event) {
  Serial.printf("[WiFi-event] event: %d\n", event);
  switch(event) {
    case SYSTEM_EVENT_STA_GOT_IP:
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
      connectToMqtt();
      waitForSync();
      timezone.setLocation(F(TIMEZONE));
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      Serial.println("WiFi lost connection");
      xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
      xTimerStart(wifiReconnectTimer, 0);
      break;
    default:
      break;
  }
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  uint16_t packetIdSubTemp = mqttClient.subscribe(MQTT_PUB_TEMP, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSubTemp);

  uint16_t packetIdSubHum = mqttClient.subscribe(MQTT_PUB_HUM, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSubHum);

  uint16_t packetIdSubDm = mqttClient.subscribe(MQTT_PUB_DM, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSubDm);

  uint16_t packetIdSubSc = mqttClient.subscribe(MQTT_PUB_SD, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSubSc);

  uint16_t packetIdSubBrightness = mqttClient.subscribe(MQTT_PUB_BRIGHTNESS, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSubBrightness);

  uint16_t packetIdSubPg = mqttClient.subscribe(MQTT_PUB_PLASMA_GRANULARITY, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSubPg);

  uint16_t packetIdSubPs = mqttClient.subscribe(MQTT_PUB_PLASMA_DELAY, 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSubPs);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");
  if (WiFi.isConnected()) {
    xTimerStart(mqttReconnectTimer, 0);
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  Serial.println("Publish received.");
  Serial.print("  topic: ");
  Serial.println(topic);
  Serial.print("  qos: ");
  Serial.println(properties.qos);
  Serial.print("  dup: ");
  Serial.println(properties.dup);
  Serial.print("  retain: ");
  Serial.println(properties.retain);
  Serial.print("  len: ");
  Serial.println(len);
  Serial.print("  index: ");
  Serial.println(index);
  Serial.print("  total: ");
  Serial.println(total);

  String strTopic = topic;

  if (strTopic.equals(MQTT_PUB_TEMP)){
    currentTemp = std::atoi(payload);
  }
  else if (strTopic.equals(MQTT_PUB_HUM)){
    currentHum = std::atoi(payload);
  }
  else if (strTopic.equals(MQTT_PUB_DM)){
    String strPayload = payload;
    if (strPayload.equals(SOLID_COLOR)){
      currentDisplayMode = DisplayMode::SolidColor;
    } 
    else if (strPayload.equals(RAINBOW)){
      currentDisplayMode = DisplayMode::Rainbow;
    } 
    else if (strPayload.equals(PLASMA)){
      currentDisplayMode = DisplayMode::Plasma;
    } 
    else if (strPayload.equals(INVERTED_SOLID_COLOR)){
      currentDisplayMode = DisplayMode::InvertedSolidColor;
    } 
    else if (strPayload.equals(INVERTED_RAINBOW)){
      currentDisplayMode = DisplayMode::InvertedRainbow;
    } 
    else if (strPayload.equals(INVERTED_PLASMA)){
      currentDisplayMode = DisplayMode::InvertedPlasma;
    }
    else if (strPayload.equals(PURE_PLASMA)){
      currentDisplayMode = DisplayMode::PurePlasma;
    }
  }
  else if (strTopic.equals(MQTT_PUB_SD)){
    String strPayload = payload;
    strip.Color(
      strPayload.substring(0, 2).toInt(),
      strPayload.substring(3, 5).toInt(),
      strPayload.substring(6, 8).toInt()
    );
  }
  else if (strTopic.equals(MQTT_PUB_BRIGHTNESS)){
    uint8_t brightness = std::atoi(payload);
    if (brightness > MAX_BRIGHTNESS){
      brightness = MAX_BRIGHTNESS;
    }
    strip.setBrightness(brightness);
  }
  else if (strTopic.equals(MQTT_PUB_PLASMA_GRANULARITY)){
    String strPayload = payload;
    if (strPayload.equals(GRAN_FINE)){
      currentGranularity = plasma::Granularity::Fine;
    }
    else if (strPayload.equals(GRAN_MEDIUM)){
      currentGranularity = plasma::Granularity::Medium;
    }
    else if (strPayload.equals(GRAN_BIG)){
      currentGranularity = plasma::Granularity::Big;
    }
  }
  else if (strTopic.equals(MQTT_PUB_PLASMA_DELAY)){
    currentDelay = std::atoi(payload);
  }
}

void setup() {
  Serial.begin(115200);
  
  mqttReconnectTimer = xTimerCreate(
    "mqttTimer", 
    pdMS_TO_TICKS(2000), 
    pdFALSE, 
    (void*)0, 
    reinterpret_cast<TimerCallbackFunction_t>(connectToMqtt));
  wifiReconnectTimer = xTimerCreate(
    "wifiTimer", 
    pdMS_TO_TICKS(2000), 
    pdFALSE, 
    (void*)0, 
    reinterpret_cast<TimerCallbackFunction_t>(connectToWifi));

  WiFi.onEvent(WiFiEvent);

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);

  mqttClient.setServer(MQTT_HOST, MQTT_PORT);

  connectToWifi();
  
  strip.begin();
  strip.setBrightness(20);
  strip.show(); // Initialize all pixels to 'off'
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void setPixel(int x, int y, colorFunc color){

  if (x >= DIMENSION_X || y >= DIMENSION_Y){
    return;
  }

  int pixelPos = DIMENSION_X * y;

  if (y % 2 == 0){
    pixelPos += DIMENSION_X - 1;
    pixelPos -= x;
  }
  else {
    pixelPos += x;
  }
  
  strip.setPixelColor(pixelPos, color(x, y));
}

void setLetter(const font::letter* val, int x, int y, colorFunc color) {

  for (int i = 0; i < font::letterWidth; i++){
    for (int j = 0; j < font::letterHeight; j++){
      if ((*val) [i][j]){
        setPixel(x + i, y + j, color);
      }
    }
  }
}

void setLetter(char val, int x, int y, colorFunc color) {
  
  setLetter(font::getLetter(val), x, y, color);
}

void setNumber(int val, int x, int y, colorFunc color) {

  setLetter(font::getNumber(val), x, y, color);  
}

void setHumidity(int val, colorFunc color){

  setNumber(val / 10, 1, 7, color);
  setNumber(val % 10, 5, 7, color);
  setLetter(&font::Percent, 9, 7, color);
}

void setTemperatur(int val, colorFunc color){

  if (val < 0){
    setLetter(&font::Dash, 1, 7, color);
    setNumber(val % 10, 5, 7, color);
    setLetter(&font::Degree, 9, 7, color);
    setLetter(&font::C, 12, 7, color);
  }
  else{
    setNumber(val / 10, 1, 7, color);
    setNumber(val % 10, 5, 7, color);
    setLetter(&font::Degree, 9, 7, color);
    setLetter(&font::C, 12, 7, color);
  }
}

void setClock(int hours, int minutes, colorFunc color){
  setNumber(hours / 10, 1, 1, color);
  setNumber(hours % 10, 4, 1, color);
  setNumber(minutes / 10, 9, 1, color);
  setNumber(minutes % 10, 12, 1, color);
}

uint32_t getBlack(int x, int y){
  return strip.Color(0, 0, 0);
}

uint32_t getSolidColor(int x, int y){
  return currentColor;
}

uint32_t getStaticRainbowColor(int x, int y){

  return Wheel((x * DIMENSION_X + y) & 255);
}

uint32_t getPlasmaColor(int x, int y){

  return myPlasma->getPixel(x, y, currentGranularity);
}

bool timeChanged(int hours, int minutes){
  return hours != currentHs || minutes != currentMins;
}

bool displayHumidity(int seconds){
  return seconds % 10 >= 5;
}

bool humidityChanged(int seconds){
  return displayHumidity(seconds) && currentHum != lastDisplayedHum;
}

bool displayTemperatur(int seconds){
  return seconds % 10 < 5;
}

bool temperaturChanged(int seconds){
  return displayTemperatur(seconds) && currentTemp != lastDisplayedTemp;
}

bool refreshDisplay(int hours, int minutes, int seconds){
  return timeChanged(hours, minutes) || humidityChanged(seconds) || temperaturChanged(seconds);
}


void setSolidColor(){

  int hours = timezone.hour();
  int minutes = timezone.minute();
  int secs = timezone.second();
  
  if (refreshDisplay(hours, minutes, secs)){
    
      strip.clear();

      setClock(hours, minutes, getSolidColor);
      currentHs = hours;
      currentMins = minutes;

      if (displayTemperatur(secs)){
        setTemperatur(currentTemp, getSolidColor);
        lastDisplayedTemp = currentTemp;
        lastDisplayedHum = UINT8_MAX;
      }
      else{
        setHumidity(currentHum, getSolidColor);
        lastDisplayedHum = currentHum;
        lastDisplayedTemp = INT8_MIN;
      }

      strip.show();
  }
}

void setInvertedSolidColor(){

  int hours = timezone.hour();
  int minutes = timezone.minute();
  int secs = timezone.second();
  
  if (refreshDisplay(hours, minutes, secs)){
    
      strip.fill(currentColor);

      setClock(hours, minutes, getBlack);
      currentHs = hours;
      currentMins = minutes;
      
      if (displayTemperatur(secs)){
        setTemperatur(currentTemp, getBlack);
        lastDisplayedTemp = currentTemp;
        lastDisplayedHum = UINT8_MAX;
      }
      else{
        setHumidity(currentHum, getBlack);
        lastDisplayedHum = currentHum;
        lastDisplayedTemp = INT8_MIN;
      }

      strip.show();
  }
}

void setRainbow() {

  int hours = timezone.hour();
  int minutes = timezone.minute();
  int secs = timezone.second();
  
  if (refreshDisplay(hours, minutes, secs)){
    
      strip.clear();

      setClock(hours, minutes, getStaticRainbowColor);
      currentHs = hours;
      currentMins = minutes;
      
      if (displayTemperatur(secs)){
        setTemperatur(currentTemp, getStaticRainbowColor);
        lastDisplayedTemp = currentTemp;
        lastDisplayedHum = UINT8_MAX;
      }
      else{
        setHumidity(currentHum, getStaticRainbowColor);
        lastDisplayedHum = currentHum;
        lastDisplayedTemp = INT8_MIN;
      }

      strip.show();
  }
}

void setInvertedRainbow() {

  int hours = timezone.hour();
  int minutes = timezone.minute();
  int secs = timezone.second();
  
  if (refreshDisplay(hours, minutes, secs)){
    
      for(int i=0; i<strip.numPixels(); i++) {
        strip.setPixelColor(i, Wheel((i) & 255));
      }

      setClock(hours, minutes, getBlack);
      currentHs = hours;
      currentMins = minutes;

      if (displayTemperatur(secs)){
        setTemperatur(currentTemp, getBlack);
        lastDisplayedTemp = currentTemp;
        lastDisplayedHum = UINT8_MAX;
      }
      else{
        setHumidity(currentHum, getBlack);
        lastDisplayedHum = currentHum;
        lastDisplayedTemp = INT8_MIN;
      }

      strip.show();
  }
}

void setPlasma(){

  if (myPlasma == 0){
    myPlasma = new plasma();
  }
  myPlasma->update();

  int hours = timezone.hour();
  int minutes = timezone.minute();
  int secs = timezone.second();
    
  strip.fill(strip.Color(0, 0, 0));

  setClock(hours, minutes, getPlasmaColor);
  currentHs = hours;
  currentMins = minutes;

  if (displayTemperatur(secs)){
    setTemperatur(currentTemp, getPlasmaColor);
    lastDisplayedTemp = currentTemp;
    lastDisplayedHum = UINT8_MAX;
  }
  else{
    setHumidity(currentHum, getPlasmaColor);
    lastDisplayedHum = currentHum;
    lastDisplayedTemp = INT8_MIN;
  }

  strip.show();
}

void setInvertedPlasma(){
  
  if (myPlasma == 0){
    myPlasma = new plasma();
  }
  myPlasma->update();

  int hours = timezone.hour();
  int minutes = timezone.minute();
  int secs = timezone.second();
  
  for (int i = 0; i < DIMENSION_X; i++){
    for (int j = 0; j < DIMENSION_Y; j++){
      setPixel(i, j, getPlasmaColor);
    }
  }

  setClock(hours, minutes, getBlack);
  currentHs = hours;
  currentMins = minutes;

  if (displayTemperatur(secs)){
    setTemperatur(currentTemp, getBlack);
    lastDisplayedTemp = currentTemp;
    lastDisplayedHum = UINT8_MAX;
  }
  else{
    setHumidity(currentHum, getBlack);
    lastDisplayedHum = currentHum;
    lastDisplayedTemp = INT8_MIN;
  }

  strip.show();
}

void setPurePlasma(){
  
  if (myPlasma == 0){
    myPlasma = new plasma();
  }
  myPlasma->update();
    
  for (int i = 0; i < DIMENSION_X; i++){
    for (int j = 0; j < DIMENSION_Y; j++){
      setPixel(i, j, getPlasmaColor);
    }
  }

  strip.show();
}

void loop() {

  events();

  switch(currentDisplayMode){
    case DisplayMode::SolidColor:
      setSolidColor();
      delay(50);
      break;
    case DisplayMode::InvertedSolidColor:
      setInvertedSolidColor();
      delay(50);
      break;
    case DisplayMode::Rainbow:
      setRainbow();
      delay(50);
      break;
    case DisplayMode::InvertedRainbow:
      setInvertedRainbow();
      delay(50);
      break;
    case DisplayMode::Plasma:
      setPlasma();
      delay(currentDelay);
      break;
    case DisplayMode::InvertedPlasma:
      setInvertedPlasma();
      delay(currentDelay);
      break;
    case DisplayMode::PurePlasma:
      setPurePlasma();
      delay(currentDelay);
      break;
  }
}