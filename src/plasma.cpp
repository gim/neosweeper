#include "plasma.h"

plasma::plasma() {
    shift = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/100);
}

uint32_t plasma::getPixel(int x, int y, Granularity gran) {

    float var1 = 1.0;
    float var2 = 1.0;
    switch (gran){
        case Granularity::Fine:
            var1 = 1.0;
            var2 = 1.0;
            break;
        case Granularity::Medium:
            var1 = 5.0;
            var2 = 7.5;
        case Granularity::Big:
            var1 = 10;
            var2 = 15;
    }

    uint16_t color = uint16_t
    ((
        offset + (offset * sin(((shift * 4.0) + (x * 2.0)) / var1))
        + offset + (offset * sin((shift + (y * 2.0)) / var1))
        + offset + (offset * sin(2 * sqrt(x*x + y*y) / var2))
        + shift/8
    ) / 3.0);
    return Adafruit_NeoPixel::ColorHSV(color);
}

void plasma::update() {
    shift += 0.035;
}
